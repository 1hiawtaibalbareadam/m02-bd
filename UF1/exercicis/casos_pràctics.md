### Habitatges  

Dissenyar un esquema E/R que reculli l'organització d'un sistema d'informació en el qual es vol tenir la informació sobre municipis, habitatges i persones. Cada persona només pot habitar en un habitatge, però pot ser propietària de més d'una. També ens interessa la interrelació de les persones amb el seu cap de família.
Feu els supòsits semàntics (`requeriments d'usuari`) complementaris necessaris.  

---  

### Gabinet d'advocats  

Es vol dissenyar una base de dades relacional per a emmagatzemar informació
sobre els assumptes que porta un gabinet d'advocats.
- Cada assumpte té un número d'expedient que l'identifica i correspon a un sol
client.
- De l'assumpte s'ha d'emmagatzemar la data d'inici, data d'arxiu (finalització)
, el seu estat (en tràmit, arxivat, etc), així com les dades personals del
client al qual pertany (DNI, nom, adreça, telèfon).
- Alguns assumptes són portats per un o diversos procuradors i viceversa, dels
quals ens interessa també les dades personals (DNI, nom, adreça, telèfon).  

---
### Zoos  
Es vol dissenyar una base de dades relacional per emmagatzemar informació relativa als zoos existents en el món, així com les espècies i animals que aquests alberguen.
- De cada zoo es vol emmagatzemar el seu codi, nom, la ciutat i país on es troba, mida (m2) i pressupost anual.
- Cada zoo codifica els seus animals amb un codi propi, de manera que entre zoos es pot donar el cas que es repeteixi.
- De cada espècie animal s'emmagatzema un codi d'espècie, el nom vulgar, el nom científic, família a la qual pertany i si es troba en perill d'extinció.
- A més, s'ha de guardar informació sobre cada animal que els zoos tenen, com el seu número d'identificació, espècie, sexe, any de naixement, país d'origen i continent.

---

### Carreteres  

Dissenyar una base de dades que contingui informació relativa a totes les carreteres d'un determinat país. Es demana realitzar el disseny en el model E/R, sabent que:

En aquest paıs les carreteres es troben dividides en trams.
Un tram sempre pertany a una única carretera i no pot canviar de carretera.
Un tram pot passar per diversos termes municipals, sent una dada d'interès al km. del tram pel qual entra en dit terme municipal i al km. pel qual surt.
Hi ha una sèrie d'àrees en les que s'agrupen els trams, cada un dels quals no pot pertànyer a més d'una àrea.

Establiu els atributs que considereu oportuns i d'ells, trieu indentificador per a cada entitat.

---

### Càtedres d'Universitat  

Dissenyar una base de dades que reculli l'organització d'una Universitat. Es considera que:

Els departaments poden estar en una sola facultat o ser interfacultatius.
Una càtedra es troba en un únic departament.
Una càtedra pertany a una sola facultat.
Un professor està sempre assignat a un únic departament i adscrit a una o diverses càtedres, podent canviar de càtedra, però no de departament. Interessa la data en què un professor és adscrit a una càtedra.
Hi ha àrees de coneixement, i tot departament tindrà una única àrea de coneixement.

---  

### Museus  

Una persona amant de l'art vol construir una base de dades de museus del món i les obres d'art de cadascú. Per les limitacions del seu equip informàtic (i les seves pròpies) va considerar únicament pintures i escultures.

Per la informació de què disposa, pot saber en quin museu està cada obra d'art i, a més es coneix la sala del museu en la qual està l'obra. Les sales dels museus tenen un nom i pot haver sales amb mateix nom en diferents museus.
Com a aficionat a la matèria que és, sap que tota obra d'art té un títol.
Altres dades són específics del tipus d'obra d'art que consideri: pintura o escultura. Així, de les pintures es coneix el seu format de anchoxalto i el tipus de pintura (oli, pastel, aquarel·la, ...). De les escultures es considera el material amb què estan fetes (bronze, ferro, marbre, ...) i l'estil de l'escultura (neoclàssica, grecoromana, cubista, ...).
Li interessarà també, conèixer els autors de les obres. Les dades generals dels autors seran el seu nom i nacionalitat. Com és natural, hi ha obres d'art de les que es desconeix l'autor.
Dels museus recollirà la següent informació: el nom del museu, direcció amb el nom del carrer i el número, a més de la ciutat i el país on està.

Nota: establiu les claus primàries que considereu oportunes.
